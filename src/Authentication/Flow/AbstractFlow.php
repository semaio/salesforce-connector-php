<?php namespace Semaio\Salesforce\Authentication\Flow;

use Semaio\Salesforce\Authentication\Config\ConfigInterface;
use Semaio\Salesforce\Authentication\Config\JwtBearerFlowConfigInterface;
use Semaio\Salesforce\Authentication\Config\PasswordFlowConfigInterface;
use Semaio\Salesforce\Exception\AuthenticationException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

/**
 * Class AbstractFlow
 *
 * @package Semaio\Salesforce\Authentication\Flow
 */
abstract class AbstractFlow implements FlowInterface
{
    /**
     * @var string
     */
    protected $loginUrl;

    /**
     * @var string
     */
    protected $apiVersion;

    /**
     * @var ConfigInterface|JwtBearerFlowConfigInterface|PasswordFlowConfigInterface
     */
    protected $config;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;

    /**
     * AbstractFlow constructor.
     *
     * @param ConfigInterface    $config
     * @param \GuzzleHttp\Client $guzzleClient
     */
    public function __construct(ConfigInterface $config, \GuzzleHttp\Client $guzzleClient)
    {
        $this->loginUrl = $config->getLoginUrl();
        $this->apiVersion = $config->getApiVersion();
        $this->config = $config;
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param string $url
     * @param array  $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws AuthenticationException
     */
    protected function makeRequest($url, $data)
    {
        try {
            return $this->guzzleClient->post($url, $data);
        } catch (GuzzleRequestException $e) {
            if ($e->getResponse() === null) {
                throw $e;
            }
            if ($e->getResponse()->getStatusCode() == 401) {
                $error = json_decode($e->getResponse()->getBody(), true);
                throw new AuthenticationException($error[0]['errorCode'], $error[0]['message']);
            }
            throw new AuthenticationException($e->getMessage(), (string)$e->getResponse()->getBody());
        }
    }
}
