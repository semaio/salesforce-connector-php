<?php namespace Semaio\Salesforce\Token\Storage;

use Semaio\Salesforce\Token\AccessToken;

/**
 * Interface StorageInterface
 *
 * @package Semaio\Salesforce\Token\Storage
 */
interface StorageInterface
{
    /**
     * Fetch the access token from the chosen storage medium and return
     *
     * @return AccessToken
     */
    public function fetchAccessToken();

    /**
     * Save an access token in the chosen store
     *
     * @param AccessToken $accessToken
     */
    public function saveAccessToken(AccessToken $accessToken);
}
