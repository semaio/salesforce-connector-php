<?php namespace Semaio\Salesforce\Exception;

/**
 * Class InvalidApiVersionException
 *
 * @package Semaio\Salesforce\Exception
 */
class InvalidApiVersionException extends \Exception
{

}
