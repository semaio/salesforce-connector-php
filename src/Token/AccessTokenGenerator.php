<?php namespace Semaio\Salesforce\Token;

use Carbon\Carbon;

/**
 * Class AccessTokenGenerator
 *
 * @package Semaio\Salesforce\Token
 */
class AccessTokenGenerator
{
    /**
     * Create an access token from stored json data
     *
     * @param $text
     * @return AccessToken
     */
    public function createFromJson($text)
    {
        $savedToken = json_decode($text, true);

        $accessToken = $savedToken['access_token'];
        $scope = $savedToken['scope'];
        $instanceUrl = $savedToken['instance_url'];
        $id = $savedToken['id'];
        $tokenType = $savedToken['token_type'];
        $dateIssued = Carbon::parse($savedToken['date_issued']);
        $dateExpires = Carbon::parse($savedToken['date_expires']);

        return new AccessToken($accessToken, $scope, $instanceUrl, $id, $tokenType, $dateIssued, $dateExpires);
    }

    /**
     * Create an access token object from the salesforce response data
     *
     * @param array $salesforceToken
     * @return AccessToken
     */
    public function createFromSalesforceResponse(array $salesforceToken)
    {
        $accessToken = $salesforceToken['access_token'];
        $scope = explode(' ', $this->getKeyIfSet($salesforceToken, 'scope'));
        $instanceUrl = $salesforceToken['instance_url'];
        $id = $this->getKeyIfSet($salesforceToken, 'id');
        $tokenType = $this->getKeyIfSet($salesforceToken, 'token_type');
        $dateIssued = Carbon::now();
        $dateExpires = $dateIssued->copy()->addHour()->subMinutes(5);

        $token = new AccessToken(
            $accessToken,
            $scope,
            $instanceUrl,
            $id,
            $tokenType,
            $dateIssued,
            $dateExpires
        );

        return $token;
    }

    /**
     * @param array $array
     * @param mixed $key
     * @return null
     */
    public function getKeyIfSet($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }

        return null;
    }
}
