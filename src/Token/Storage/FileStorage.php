<?php namespace Semaio\Salesforce\Token\Storage;

use Semaio\Salesforce\Token\AccessToken;
use Semaio\Salesforce\Token\AccessTokenGenerator;

/**
 * Class FileStorage
 *
 * @package Semaio\Salesforce\Token\Storage
 */
class FileStorage implements StorageInterface
{
    /**
     * @var AccessTokenGenerator
     */
    private $accessTokenGenerator;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @param AccessTokenGenerator $accessTokenGenerator
     * @param FileStorageInterface $config
     */
    public function __construct(AccessTokenGenerator $accessTokenGenerator, FileStorageInterface $config)
    {
        $this->accessTokenGenerator = $accessTokenGenerator;
        $this->fileName = $config->getFileName();
        $this->filePath = $config->getFilePath();
    }

    /**
     * @return AccessToken
     * @throws \Exception
     */
    public function fetchAccessToken()
    {
        try {
            if (!file_exists($this->filePath . DIRECTORY_SEPARATOR . $this->fileName)) {
                throw new \Exception();
            }
            $accessTokenJson = file_get_contents($this->filePath . DIRECTORY_SEPARATOR . $this->fileName);
            if (trim($accessTokenJson) == '') {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            throw new \Exception('Salesforce access token not found');
        }

        return $this->accessTokenGenerator->createFromJson($accessTokenJson);
    }

    /**
     * @param AccessToken $accessToken
     */
    public function saveAccessToken(AccessToken $accessToken)
    {
        file_put_contents($this->filePath . DIRECTORY_SEPARATOR . $this->fileName, $accessToken->toJson());
    }
}

