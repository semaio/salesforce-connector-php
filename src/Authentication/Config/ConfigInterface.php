<?php namespace Semaio\Salesforce\Authentication\Config;

/**
 * Interface ConfigInterface
 *
 * @package Semaio\Salesforce\Authentication\Config
 */
interface ConfigInterface
{
    /**
     * @return string
     */
    public function getLoginUrl();

    /**
     * @return string
     */
    public function getClientId();

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @return string
     */
    public function getApiVersion();
}
