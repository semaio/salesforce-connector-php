<?php namespace Semaio\Salesforce\Authentication\Config;

/**
 * Class JwtBearerFlowConfig
 *
 * @package Semaio\Salesforce\Authentication\Config
 */
class JwtBearerFlowConfig implements JwtBearerFlowConfigInterface
{
    /**
     * @var string
     */
    private $loginUrl;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $privateKeyFile;

    /**
     * @var string
     */
    private $apiVersion;

    /**
     * ClientConfig constructor.
     *
     * @param string $loginUrl
     * @param string $clientId
     * @param string $username
     * @param string $privateKeyFile
     * @param string $apiVersion
     */
    public function __construct($loginUrl, $clientId, $username, $privateKeyFile, $apiVersion)
    {
        $this->loginUrl = $loginUrl;
        $this->clientId = $clientId;
        $this->username = $username;
        $this->privateKeyFile = $privateKeyFile;
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPrivateKeyFile()
    {
        return $this->privateKeyFile;
    }

    /**
     * @return string
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }
}
