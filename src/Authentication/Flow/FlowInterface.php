<?php namespace Semaio\Salesforce\Authentication\Flow;

/**
 * Interface FlowInterface
 *
 * @package Semaio\Salesforce\Authentication\Flow
 */
interface FlowInterface
{
    /**
     * Retrieve the access token.
     *
     * @return \Semaio\Salesforce\Token\AccessToken
     */
    public function getAccessToken();
}
