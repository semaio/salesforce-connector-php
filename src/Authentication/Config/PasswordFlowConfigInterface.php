<?php namespace Semaio\Salesforce\Authentication\Config;

/**
 * Interface PasswordFlowConfigInterface
 *
 * @package Semaio\Salesforce\Authentication\Config
 */
interface PasswordFlowConfigInterface extends ConfigInterface
{
    /**
     * @return string
     */
    public function getClientSecret();

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @return string
     */
    public function getSecurityToken();
}
