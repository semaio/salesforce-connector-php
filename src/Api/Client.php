<?php

namespace Semaio\Salesforce\Api;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use Semaio\Salesforce\Exception\CompositeLogicException;
use Semaio\Salesforce\Exception\InvalidApiVersionException;
use Semaio\Salesforce\Exception\RequestException;
use Semaio\Salesforce\Token\AccessToken;

class Client
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const METHOD_PUT = 'put';
    const METHOD_PATCH = 'patch';
    const METHOD_DELETE = 'delete';

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;

    /**
     * @var string
     */
    private $apiVersion;

    /**
     * @var AccessToken
     */
    private $accessToken;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $instanceUrl;

    /**
     * Create a Salesforce client using a dedicated authentication flow, a client config object and a HTTP client.
     *
     * @param string $apiVersion
     */
    public function __construct(AccessToken $accessToken, $apiVersion)
    {
        $this->guzzleClient = new \GuzzleHttp\Client();
        $this->apiVersion = $apiVersion;
        $this->setAccessToken($accessToken);
    }

    /**
     * Retrieve a list of all API Versions for the instance.
     *
     * @return mixed
     *
     * @throws RequestException
     */
    public function getApiVersions()
    {
        $response = $this->makeRequest(self::METHOD_GET, $this->baseUrl.'/services/data', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthHeader(),
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Retrieve the limits for the organization.
     *
     * @return mixed
     *
     * @throws RequestException
     */
    public function getOrgLimits()
    {
        $response = $this->makeRequest(self::METHOD_GET, $this->instanceUrl.'/limits', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthHeader(),
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Retrieve a list of all available REST resources for the organization.
     *
     * @return mixed
     *
     * @throws RequestException
     */
    public function getAvailableResources()
    {
        $response = $this->makeRequest(self::METHOD_GET, $this->instanceUrl, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthHeader(),
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Retrieve a list of all available objects for the organization.
     *
     * @return mixed
     *
     * @throws RequestException
     */
    public function getAllObjects()
    {
        $response = $this->makeRequest(self::METHOD_GET, $this->instanceUrl.'/sobjects', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthHeader(),
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param string $objectName
     * @param bool   $all
     *
     * @return mixed
     *
     * @throws RequestException
     */
    public function getObjectMetadata($objectName, $all = false, \DateTime $since = null)
    {
        $headers = [];

        // Check if the If-Modified-Since header should be set
        if ($since !== null && $since instanceof \DateTime) {
            $headers['IF-Modified-Since'] = $since->format('D, j M Y H:i:s e');
        } elseif ($since !== null && !$since instanceof \DateTime) {
            // If the $since flag has been set and is not a DateTime instance, throw an error
            throw new \InvalidArgumentException('To get object metadata for an object, you must provide a DateTime object');
        }

        // Should this return all meta data including information about each field, URLs, and child relationships
        if ($all === true) {
            $url = $this->instanceUrl.'/sobjects/'.$objectName.'/describe/';
        } else {
            $url = $this->instanceUrl.'/sobjects/'.$objectName.'/';
        }

        $headers = array_merge($headers, [
            'Content-Type' => 'application/json',
            'Authorization' => $this->getAuthHeader(),
        ]);

        $response = $this->makeRequest(self::METHOD_GET, $url, [
            'headers' => $headers,
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Create a new object in salesforce.
     *
     * @param string $object
     * @param string $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function createRecord($object, $data)
    {
        $response = $this->makeRequest(self::METHOD_POST, $this->instanceUrl.'/sobjects/'.$object.'/', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->getAuthHeader(),
            ],
            'body' => json_encode($data),
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Fetch a specific object.
     *
     * @param string $objectType
     * @param string $sfId
     * @param array  $fields
     *
     * @return string
     *
     * @throws RequestException
     */
    public function getRecord($objectType, $sfId, array $fields = null)
    {
        $body = [];
        if ($fields !== null && is_array($fields)) {
            $body['fields'] = implode(',', $fields);
        }

        $requestData = [
            'headers' => [
                'Authorization' => $this->getAuthHeader(),
            ],
        ];

        if (count($body)) {
            $requestData['body'] = json_encode($body);
        }

        $response = $this->makeRequest(
            self::METHOD_GET,
            $this->instanceUrl.'/sobjects/'.$objectType.'/'.$sfId,
            $requestData
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Make an upsert request.
     *
     * @param string $object The object type to update
     * @param array  $data   The data to put into the record
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function upsertRecord($object, array $data = null)
    {
        $response = $this->makeRequest(
            self::METHOD_PATCH,
            $this->instanceUrl.'/sobjects/'.$object,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getAuthHeader(),
                ],
                'body' => json_encode($data),
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Make an update request.
     *
     * @param string $object The object type to update
     * @param string $id     The ID of the record to update
     * @param array  $data   The data to put into the record
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function updateRecord($object, $id, array $data = null)
    {
        $response = $this->makeRequest(
            self::METHOD_PATCH,
            $this->instanceUrl.'/sobjects/'.$object.'/'.$id,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getAuthHeader(),
                ],
                'body' => json_encode($data),
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Delete an object with th specified id.
     *
     * @param $object
     * @param $id
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function deleteRecord($object, $id)
    {
        $response = $this->makeRequest(
            self::METHOD_DELETE,
            $this->instanceUrl.'/sobjects/'.$object.'/'.$id,
            [
                'headers' => [
                    'Authorization' => $this->getAuthHeader(),
                ],
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Create multiple records in one request.
     *
     * @param bool $allOrNone
     *
     * @return mixed
     *
     * @throws RequestException
     * @throws InvalidApiVersionException
     * @throws CompositeLogicException
     */
    public function compositeCreateRecord(array $records, $allOrNone = false)
    {
        if (intval($this->apiVersion) < 42) {
            throw new InvalidApiVersionException('Composite requests are supported from API version v42.0, you are on v'.$this->apiVersion);
        }
        if (count($records) > 200) {
            throw new CompositeLogicException('Only up to 200 objects can be created in one request.');
        }

        $body = [
            'records' => $records,
            'allOrNone' => $allOrNone,
        ];

        $response = $this->makeRequest(
            self::METHOD_POST,
            $this->instanceUrl.'/composite/sobjects',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getAuthHeader(),
                ],
                'body' => json_encode($body),
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Retrieve multiple records of a given object type in one request.
     *
     * @param string $objectType
     *
     * @return mixed
     *
     * @throws RequestException
     * @throws InvalidApiVersionException
     */
    public function compositeGetRecord($objectType, array $ids, array $fields)
    {
        if (intval($this->apiVersion) < 42) {
            throw new InvalidApiVersionException('Composite requests are supported from API version v42.0, you are on v'.$this->apiVersion);
        }

        $body = [
            'ids' => $ids,
            'fields' => $fields,
        ];

        $response = $this->makeRequest(
            self::METHOD_GET,
            $this->instanceUrl.'/composite/sobjects/'.$objectType,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getAuthHeader(),
                ],
                'body' => json_encode($body),
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Update multiple records in one request.
     *
     * @param bool $allOrNone
     *
     * @return mixed
     *
     * @throws CompositeLogicException
     * @throws InvalidApiVersionException
     * @throws RequestException
     */
    public function compositeUpdateRecord(array $records, $allOrNone = false)
    {
        if (intval($this->apiVersion) < 42) {
            throw new InvalidApiVersionException('Composite requests are supported from API version v42.0, you are on v'.$this->apiVersion);
        }
        if (count($records) > 200) {
            throw new CompositeLogicException('Only up to 200 objects can be updated in one request.');
        }

        $body = [
            'records' => $records,
            'allOrNone' => $allOrNone,
        ];

        $response = $this->makeRequest(
            self::METHOD_PATCH,
            $this->instanceUrl.'/composite/sobjects',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getAuthHeader(),
                ],
                'body' => json_encode($body),
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Delete multiple records in one request.
     *
     * @return mixed
     *
     * @throws RequestException
     * @throws InvalidApiVersionException
     * @throws CompositeLogicException
     */
    public function compositeDeleteRecord(array $ids)
    {
        if (intval($this->apiVersion) < 42) {
            throw new InvalidApiVersionException('Composite requests are supported from API version v42.0, you are on v'.$this->apiVersion);
        }
        if (count($ids) > 200) {
            throw new CompositeLogicException('Only up to 200 objects can be deleted in one request.');
        }

        $response = $this->makeRequest(
            self::METHOD_DELETE,
            $this->instanceUrl.'/composite/sobjects?ids='.implode(',', array_map('trim', $ids)),
            [
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->getAuthHeader()],
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Execute an SOQL query and return the result set
     * This will loop through large result sets collecting all the data so the query should be limited.
     *
     * @param string|null $query
     * @param string|null $nextUrl
     * @param bool        $allRecords
     * @param bool        $explainQuery
     *
     * @return array
     *
     * @throws RequestException
     */
    public function search($query = null, $nextUrl = null, $allRecords = false, $explainQuery = false)
    {
        if (!empty($nextUrl)) {
            $url = $this->baseUrl.'/'.$nextUrl;
        } else {
            $path = 'query';
            if ($allRecords) {
                $path = 'queryAll';
            }

            if ($explainQuery) {
                $url = $this->instanceUrl.'/'.$path.'/?explain='.urlencode($query);
            } else {
                $url = $this->instanceUrl.'/'.$path.'/?q='.urlencode($query);
            }
        }

        $response = $this->makeRequest(self::METHOD_GET, $url, [
            'headers' => ['Authorization' => $this->getAuthHeader()],
        ]);
        $data = json_decode($response->getBody(), true);

        $results = $data['records'];
        if (!$data['done']) {
            $moreResults = $this->search(null, substr($data['nextRecordsUrl'], 1));
            if (!empty($moreResults)) {
                $results = array_merge($results, $moreResults);
            }
        }

        return $results;
    }

    public function request($method, $url, array $data = [], array $headers = [])
    {
        $headers = array_merge($headers, [
            'Authorization' => $this->getAuthHeader(),
        ]);

        $response = $this->makeRequest($method, $this->baseUrl.'/'.$url, [
            'headers' => $headers,
            'body' => json_encode($data),
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $data
     *
     * @return \GuzzleHttp\Psr7\Response
     *
     * @throws RequestException
     */
    private function makeRequest($method, $url, $data)
    {
        try {
            return $this->guzzleClient->$method($url, $data);
        } catch (GuzzleRequestException $e) {
            if ($e->getResponse() === null) {
                throw $e;
            }

            if ($e->getResponse()->getStatusCode() == 401) {
                $error = json_decode($e->getResponse()->getBody(), true);
                throw new RequestException($error[0]['errorCode'], $error[0]['message']);
            }

            throw new RequestException($e->getMessage(), (string) $e->getResponse()->getBody());
        }
    }

    /**
     * @return string
     *
     * @throws RequestException
     */
    private function getAuthHeader()
    {
        if ($this->accessToken === null) {
            throw new RequestException(0, 'Access token not set');
        }

        return 'Bearer '.$this->accessToken->getAccessToken();
    }

    public function setAccessToken(AccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
        $this->baseUrl = $accessToken->getInstanceUrl();
        $this->instanceUrl = $this->baseUrl.'/services/data/v'.$this->apiVersion;
    }

    /**
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }
}
