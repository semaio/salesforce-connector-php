<?php namespace Semaio\Salesforce\Token\Generator;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key;

/**
 * Class JwtTokenGenerator
 *
 * @package Semaio\Salesforce\Token\Generator
 */
class JwtTokenGenerator
{
    /**
     * @param string $clientId
     * @param string $username
     * @param string $loginUrl
     * @param string $privateKeyFile
     * @return string
     */
    public static function build($clientId, $username, $loginUrl, $privateKeyFile)
    {
        $payload = [
            'iss' => $clientId,
            'sub' => $username,
            'aud' => rtrim($loginUrl, '/'),
            'exp' => time() + 30,
            'iat' => time(),
        ];

        $signer = self::signerFactory('RS256');
        $privateKey = new Key(file_get_contents($privateKeyFile), null);
        $builder = self::tokenBuilderFactory($payload);
        $token = $builder->sign($signer, $privateKey)->getToken();

        return (string)$token;
    }

    /**
     * @param array $payload
     * @return Builder
     */
    protected static function tokenBuilderFactory($payload)
    {
        $token = new Builder();
        foreach ($payload as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $sub_value) {
                    $token->set($key, $sub_value);
                }
            } else {
                $token->set($key, $value);
            }
        }

        return $token;
    }

    /**
     * @param string $algorithm
     * @return Signer
     */
    protected static function signerFactory($algorithm)
    {
        switch ($algorithm) {
            case 'RS256':
            default:
                return new \Lcobucci\JWT\Signer\Rsa\Sha256();
        }
    }
}
