<?php namespace Semaio\Salesforce\Token\Storage;

/**
 * Interface FileStorageInterface
 *
 * @package Semaio\Salesforce\Token\Storage
 */
interface FileStorageInterface
{
    /**
     * The name of the file und which it will be stored
     *
     * @return string
     */
    public function getFileName();

    /**
     * The path where the file will be stored, no trailing slash, must be writable
     *
     * @return string
     */
    public function getFilePath();
}
