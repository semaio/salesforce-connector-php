<?php namespace Semaio\Salesforce\Token;

class AccessTokenGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AccessTokenGenerator
     */
    private $accessTokenGenerator;

    protected function setUp()
    {
        $this->accessTokenGenerator = (new AccessTokenGenerator());
    }

    public function testCreateFromJson()
    {
        $json = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . '_data' . DIRECTORY_SEPARATOR . 'token.json');

        $accessToken = $this->accessTokenGenerator->createFromJson($json);
        $this->assertInstanceOf('\Semaio\Salesforce\Token\AccessToken', $accessToken);
    }

    public function testCreateFromSalesforceResponse()
    {
        $response = [
            'access_token' => 'xxxxxxx',
            'scope'        => 'web full',
            'instance_url' => 'https://cs000.salesforce.com',
            'id'           => 'https://test.salesforce.com/id/xxxxxxx',
            'token_type'   => 'Bearer',
            'date_issued'  => '2018-09-01 00:00:00',
            'date_expires' => '2018-09-01 00:55:00',
        ];

        $accessToken = $this->accessTokenGenerator->createFromSalesforceResponse($response);
        $this->assertInstanceOf('\Semaio\Salesforce\Token\AccessToken', $accessToken);
    }

    public function testGetKeyIfSet()
    {
        $array = [
            'key1' => 'val1',
        ];

        $this->assertEquals('val1', $this->accessTokenGenerator->getKeyIfSet($array, 'key1'));
        $this->assertNull($this->accessTokenGenerator->getKeyIfSet($array, 'key2'));
    }
}
