<?php namespace Semaio\Salesforce\Authentication\Flow;

use Semaio\Salesforce\Authentication\Config\PasswordFlowConfig;
use Semaio\Salesforce\Token\AccessTokenGenerator;

/**
 * Class PasswordFlow
 *
 * @package Semaio\Salesforce\Authentication\Flow
 */
class PasswordFlow extends AbstractFlow
{
    /**
     * Create an authentication flow instance to retreive the access token using the passed in config data.
     *
     * @param string $loginUrl
     * @param string $clientId
     * @param string $clientSecret
     * @param string $username
     * @param string $password
     * @param string $securityToken
     * @param string $apiVersion
     * @return PasswordFlow
     */
    public static function create($loginUrl, $clientId, $clientSecret, $username, $password, $securityToken, $apiVersion)
    {
        return new self(
            new PasswordFlowConfig($loginUrl, $clientId, $clientSecret, $username, $password, $securityToken, $apiVersion),
            new \GuzzleHttp\Client
        );
    }

    /**
     * Retrieve the access token.
     *
     * @return \Semaio\Salesforce\Token\AccessToken
     * @throws \Semaio\Salesforce\Exception\AuthenticationException
     */
    public function getAccessToken()
    {
        $postData = [
            'grant_type'    => 'password',
            'client_id'     => $this->config->getClientId(),
            'client_secret' => $this->config->getClientSecret(),
            'username'      => $this->config->getUsername(),
            'password'      => $this->config->getPassword() . $this->config->getSecurityToken(),
        ];

        $url = $this->loginUrl . '/services/oauth2/token';

        $response = $this->makeRequest($url, ['form_params' => $postData]);
        $response = json_decode($response->getBody(), true);

        return (new AccessTokenGenerator())->createFromSalesforceResponse($response);
    }
}
