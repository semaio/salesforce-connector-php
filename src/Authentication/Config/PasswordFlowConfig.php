<?php namespace Semaio\Salesforce\Authentication\Config;

/**
 * Class PasswordFlowConfig
 *
 * @package Semaio\Salesforce\Authentication\Config
 */
class PasswordFlowConfig implements PasswordFlowConfigInterface
{
    /**
     * @var string
     */
    private $loginUrl;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $securityToken;

    /**
     * @var string
     */
    private $apiVersion;

    /**
     * ClientConfig constructor.
     *
     * @param string $loginUrl
     * @param string $clientId
     * @param string $clientSecret
     * @param string $username
     * @param string $password
     * @param string $securityToken
     * @param string $apiVersion
     */
    public function __construct($loginUrl, $clientId, $clientSecret, $username, $password, $securityToken, $apiVersion)
    {
        $this->loginUrl = $loginUrl;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->username = $username;
        $this->password = $password;
        $this->securityToken = $securityToken;
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getSecurityToken()
    {
        return $this->securityToken;
    }

    /**
     * @return string
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }
}
