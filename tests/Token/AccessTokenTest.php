<?php namespace Semaio\Salesforce\Token;

use Carbon\Carbon;

class AccessTokenTest extends \PHPUnit_Framework_TestCase
{
    public function testNeedsRefresh()
    {
        $token = $this->getAccessToken('2018-09-01 00:00:00', '2018-09-01 00:55:00');
        $this->assertTrue($token->needsRefresh());
    }

    public function testNoNeedRefresh()
    {
        $token = $this->getAccessToken();
        $this->assertFalse($token->needsRefresh());
    }

    public function testToArray()
    {
        $token = $this->getAccessToken('2018-09-01 00:00:00', '2018-09-01 00:55:00');
        $this->assertEquals([
            'access_token' => 'xxxxxxx',
            'scope'        => ['web', 'full'],
            'instance_url' => 'https://cs000.salesforce.com',
            'id'           => 'https://test.salesforce.com/id/xxxxxxx',
            'token_type'   => 'Bearer',
            'date_issued'  => '2018-09-01 00:00:00',
            'date_expires' => '2018-09-01 00:55:00',

        ], $token->toArray());
    }

    public function testToString()
    {
        $token = $this->getAccessToken('2018-09-01 00:00:00', '2018-09-01 00:55:00');
        $this->assertEquals(
            '{"access_token":"xxxxxxx","scope":["web","full"],"instance_url":"https:\/\/cs000.salesforce.com","id":"https:\/\/test.salesforce.com\/id\/xxxxxxx","token_type":"Bearer","date_issued":"2018-09-01 00:00:00","date_expires":"2018-09-01 00:55:00"}',
            $token->__toString()
        );
    }

    public function testGetters()
    {
        $token = $this->getAccessToken('2018-09-01 00:00:00', '2018-09-01 00:55:00');

        $this->assertEquals('xxxxxxx', $token->getAccessToken());
        $this->assertEquals(['web', 'full'], $token->getScope());
        $this->assertEquals('https://cs000.salesforce.com', $token->getInstanceUrl());
        $this->assertEquals('https://test.salesforce.com/id/xxxxxxx', $token->getId());
        $this->assertEquals('Bearer', $token->getTokenType());

        $this->assertInstanceOf('Carbon\Carbon', $token->getDateIssued());
        $this->assertInstanceOf('Carbon\Carbon', $token->getDateExpires());

    }


    private function getAccessToken($dateIssued = null, $dateExpires = null)
    {
        if (null !== $dateIssued) {
            $dateIssued = Carbon::parse($dateIssued);
        }
        if (null !== $dateExpires) {
            $dateExpires = Carbon::parse($dateExpires);
        }

        return new AccessToken(
            'xxxxxxx',
            ['web', 'full'],
            'https://cs000.salesforce.com',
            'https://test.salesforce.com/id/xxxxxxx',
            'Bearer',
            $dateIssued,
            $dateExpires
        );
    }
}
