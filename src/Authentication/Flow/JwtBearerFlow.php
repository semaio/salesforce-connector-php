<?php namespace Semaio\Salesforce\Authentication\Flow;

use Semaio\Salesforce\Authentication\Config\JwtBearerFlowConfig;
use Semaio\Salesforce\Token\AccessTokenGenerator;
use Semaio\Salesforce\Token\Generator\JwtTokenGenerator;

/**
 * Class JwtBearerFlow
 *
 * @package Semaio\Salesforce\Authentication\Flow
 */
class JwtBearerFlow extends AbstractFlow
{
    /**
     * Create an authentication flow instance to retreive the access token using the passed in config data.
     *
     * @param string $loginUrl
     * @param string $clientId
     * @param string $username
     * @param string $privateKeyFile
     * @param string $apiVersion
     * @return JwtBearerFlow
     */
    public static function create($loginUrl, $clientId, $username, $privateKeyFile, $apiVersion)
    {
        return new self(
            new JwtBearerFlowConfig($loginUrl, $clientId, $username, $privateKeyFile, $apiVersion),
            new \GuzzleHttp\Client
        );
    }

    /**
     * Retrieve the access token.
     *
     * @return \Semaio\Salesforce\Token\AccessToken
     * @throws \Semaio\Salesforce\Exception\AuthenticationException
     */
    public function getAccessToken()
    {
        $postData = [
            'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'assertion'  => (new JwtTokenGenerator())->build(
                $this->config->getClientId(),
                $this->config->getUsername(),
                $this->loginUrl,
                $this->config->getPrivateKeyFile()
            ),
        ];

        $url = $this->loginUrl . '/services/oauth2/token';

        $response = $this->makeRequest($url, ['form_params' => $postData]);
        $response = json_decode($response->getBody(), true);

        return (new AccessTokenGenerator())->createFromSalesforceResponse($response);
    }
}
