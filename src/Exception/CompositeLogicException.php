<?php namespace Semaio\Salesforce\Exception;

/**
 * Class CompositeLogicException
 *
 * @package Semaio\Salesforce\Exception
 */
class CompositeLogicException extends \Exception
{

}
