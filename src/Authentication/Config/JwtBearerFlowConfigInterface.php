<?php namespace Semaio\Salesforce\Authentication\Config;

/**
 * Interface JwtBearerFlowConfigInterface
 *
 * @package Semaio\Salesforce\Authentication\Config
 */
interface JwtBearerFlowConfigInterface extends ConfigInterface
{
    /**
     * @return string
     */
    public function getPrivateKeyFile();
}
