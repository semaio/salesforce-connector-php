<?php namespace Semaio\Salesforce\Token;

use Carbon\Carbon;

/**
 * Class AccessToken
 *
 * @package Semaio\Salesforce\Token
 */
class AccessToken
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var array
     */
    private $scope;

    /**
     * @var string
     */
    private $instanceUrl;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $tokenType;

    /**
     * @var Carbon
     */
    private $dateIssued;

    /**
     * @var Carbon
     */
    private $dateExpires;

    /**
     * AccessToken constructor.
     *
     * @param string      $accessToken
     * @param array       $scope
     * @param string      $instanceUrl
     * @param string      $id
     * @param string      $tokenType
     * @param string|null $dateIssued
     * @param string|null $dateExpires
     */
    public function __construct($accessToken, array $scope, $instanceUrl, $id, $tokenType, $dateIssued = null, $dateExpires = null)
    {
        $this->accessToken = $accessToken;
        $this->scope = $scope;
        $this->instanceUrl = $instanceUrl;
        $this->id = $id;
        $this->tokenType = $tokenType;

        if (null === $dateIssued) {
            $dateIssued = Carbon::now();
        }
        $this->dateIssued = $dateIssued;

        if (null === $dateExpires) {
            $dateExpires = $this->dateIssued->copy()->addHour()->subMinutes(5);
        }
        $this->dateExpires = $dateExpires;
    }

    /**
     * @return bool
     */
    public function needsRefresh()
    {
        return $this->dateExpires->lt(Carbon::now());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'access_token' => $this->accessToken,
            'scope'        => $this->scope,
            'instance_url' => $this->instanceUrl,
            'id'           => $this->id,
            'token_type'   => $this->tokenType,
            'date_issued'  => $this->dateIssued->format('Y-m-d H:i:s'),
            'date_expires' => $this->dateExpires->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return array
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return string
     */
    public function getInstanceUrl()
    {
        return $this->instanceUrl;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @return Carbon
     */
    public function getDateIssued()
    {
        return $this->dateIssued;
    }

    /**
     * @return Carbon
     */
    public function getDateExpires()
    {
        return $this->dateExpires;
    }
}
